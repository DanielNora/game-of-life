Students:
    - Daniel Nora Castro (12/0114470) @d1ngell
    - Miguel Freitas (12/0130424) @MiguelbrmFreitas

Description:
This program has been created as a solution for the exercise proposed in the Object-Oriented Programming course taught by professor Rodrigo Bonifacio at Universidade de Brasília. It is the implementation of the Game of Life game employing some design patterns to solve the flaws that existed in a previously specified design.

Proposed solutions:
In this solution, the design flaws we chose to correct were:
    - Separation of Concerns in a more adequate way (employing the MVC architectural pattern)
    - Implementation of the Strategy design pattern to support different variations of the Game of Life game in order to allow that such variations be modified at runtime
    - Implementation of the Observer design pattern, for the views, that shall reflect the contents of the model as it is modified by the controller, as for the Statistics, that shall be responsible for producing a message containing the number of cells that were killed or ressurrected in the model, for exhibition in a view component.
    - Implementation of a Graphical User Interface using the standard graphical Java API (Swing), allowing for the existence of both a CLI and GUI.

Operation:
To run the software, it's enough to compile the code using an Integrated Development Environment (Eclipse, Netbeans, etc). The software then will request the amount of rows and columns the Game of Life board shall be created with, and then the desired view for the application (Console or GUI).

--------------------------------------------------------------------------------

Alunos:
      - Daniel Nora Castro (12/0114470) @d1ngell
      - Miguel Freitas (12/0130424) @MiguelbrmFreitas

Descricao:
Este programa foi criado como uma resolucao da atividade proposta na disciplina de Programacao Orientada a Objetos da Universidade de Brasilia. Trata-se da implementacao do jogo Game of Life empregando alguns padroes de projeto de software para corrigir falhas num design previamente especificado.

Solucoes propostas:
Nesta solucao, as falhas que optamos por corrigir foram:
     - Separacao de Responsabilidades de maneira mais adequada (empregando o padrao arquitetural MVC)
     - Implementacao do padrao de projeto Strategy para suportar diferentes variacoes do jogo Game of Life e permitir que essas variacoes sejam empregadas em tempo de execucao
     - Implementacao do padrao de projeto Observer, tanto para as views, que refletirao o conteudo da model a medida que esta vai sendo modificada pela controller, quanto para as Statistics, que deve ser responsavel por produzir uma mensagem contendo o numero de celulas que foram mortas ou ressuscitadas na model, para exibicao num componente de apresentacao.
     - Implementacao da Interface Grafica utilizando a API padrao do Java, Swing, possibilitando a coexistencia desta com a apresentacao em Shell.

Funcionamento:
Para utilizar o programa, basta compilar o codigo utilizando um Ambiente de Desenvolvimento Integrado (Eclipse, Netbeans, etc). O programa entao solicita o numero de linhas e de colunas do tabuleiro Game of Life a ser criado, e entao o tipo de apresentacao que o usuario deseja utilizar (Interface Grafica ou Console).
