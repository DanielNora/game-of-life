package mvc.controller;

import mvc.model.GameEngine;
import mvc.model.Statistics;
import mvc.model.Strategies;

public class GameController {

	private GameEngine engine;
	private Statistics statistics;
	
	public void setEngine(GameEngine engine) {
		this.engine = engine;
	}
	
	public int getBoardWidth() {
		return engine.getWidth();
	}

	public int getBoardHeight() {
		return engine.getHeight();
	}
	
	public boolean isCellAlive(int i, int j) {
		return engine.isCellAlive(i, j);
	}
	
	public void makeCellAlive(int i, int j) {
		engine.makeCellAlive(i, j);
	}

	public void changeStrategy(Strategies strategy) {
		engine.setStrategy(Strategies.fabricate(strategy));
	}

	public void animation(int geracoes) {
		for ( int i = 0; i < geracoes; i++ ) {
			if (engine.numberOfAliveCells() > 0) {
				nextGeneration();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else break;
		}
	}

	public void killAllCells() {
		engine.killAllCells();
	}

	public void exit() {
		System.exit(0);
	}

	public String getStatistics() {
		return statistics.getMessage();
	}

	public void nextGeneration() {
		engine.nextGeneration();
	}

	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}

	public void makeCellDead(int i, int j) {
		engine.makeCellDead(i, j);		
	}
	
	public int numberOfAliveCells() {
		return engine.numberOfAliveCells();
	}
	
}
