package mvc.model;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import mvc.model.strategies.Conway;
import observer.Observer;
import observer.Subject;

public class GameEngine implements Subject {

	private List<Observer> observers = new ArrayList<Observer>();

	private int height;
	private int width;
	private Cell[][] cells;

	private GameStrategy strategy;

	private int revivedCells = 0;
	private int killedCells = 0;

	/**
	 * GameEngine class constructor.
	 *
	 * @param height
	 *            vertical dimension of the board
	 * @param width
	 *            horizontal dimension of the board
	 */
	public GameEngine(int height, int width) {
		this.height = height;
		this.width = width;

		cells = new Cell[height][width];

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				cells[i][j] = new Cell();
			}
		}

		strategy = new Conway(); // Sets the initial strategy
		strategy.setEngine(this);
	}

	/**
	 * Calculates a new generation. This implementation is somewhat flexible
	 * and allows for different rules and strategies for the game.
	 */
	public void nextGeneration() {
		List<Cell> mustRevive = new ArrayList<Cell>();
		List<Cell> mustKill = new ArrayList<Cell>();
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (strategy.shouldRevive(i, j)) {
					mustRevive.add(cells[i][j]);
				}
				else if ((!strategy.shouldKeepAlive(i, j)) && cells[i][j].isAlive()) {
					mustKill.add(cells[i][j]);
				}
			}
		}

		for (Cell cell : mustRevive) {
			cell.revive();
			revivedCells++;
		}

		for (Cell cell : mustKill) {
			cell.kill();
			killedCells++;
		}
		notifyObservers();
	}

	/**
	 * Ressurrects the cell at (i, j)
	 *
	 * @param i vertical position of the cell
	 * @param j horizontal position of the cell
	 *
	 * @throws InvalidParameterException if the position (i, j) is not valid
	 */
	public void makeCellAlive(int i, int j) throws InvalidParameterException {
		if(validPosition(i, j)) {
			cells[i][j].revive();
			revivedCells++;
			notifyObservers();
		}
		else {
			new InvalidParameterException("Invalid position (" + i + ", " + j + ")" );
		}
	}

	public void makeCellDead(int i, int j) throws InvalidParameterException {
		if(validPosition(i, j)) {
			cells[i][j].kill();
			killedCells++;
			notifyObservers();
		}
		else {
			new InvalidParameterException("Invalid position (" + i + ", " + j + ")" );
		}
	}

	/**
	 * Kills all game cells
	 */
	public void killAllCells(){
		for(int i = 0;  i < height; i++){
			for(int j = 0; j < width; j++){
				cells[i][j].kill();
			}
		}
		notifyObservers();
	}

	/**
	 * Checks whether a cell at (i, j) is alive
	 *
	 * @param i Vertical position of the cell
	 * @param j Horizontal position of the cell
	 * @return True if the cell at position (i, j) is alive
	 *
	 * @throws InvalidParameterException if the position (i,j) is not valid.
	 */
	public boolean isCellAlive(int i, int j) throws InvalidParameterException {
		if(validPosition(i, j)) {
			return cells[i][j].isAlive();
		}
		else {
			throw new InvalidParameterException("Invalid position (" + i + ", " + j + ")" );
		}
	}

	/**
	 * Returns the number of alive cells in the board.
	 * This method is particularly useful in calculating the
	 * statistics and for making better tests.
	 *
	 * @return number of alive cells
	 */
	public int numberOfAliveCells() {
		int aliveCells = 0;
		for(int i = 0; i < height; i++) {
			for(int j = 0; j < width; j++) {
				if(isCellAlive(i,j)) {
					aliveCells++;
				}
			}
		}
		return aliveCells;
	}

	public int getRevivedCells() {
		return revivedCells;
	}
	public int getKilledCells() {
		return killedCells;
	}

	/**
	 * Makes the infinite world possible (cells overlap the borders)
	 * @param x number to be normalized
	 * @return
	 */
	public int normalize(int x){
		if(x >= height){
			return x - height;
		}
		else if (x < 0){
			return x + height;
		}

		return x;
	}

	/**
	 * Calculates the number of alive neighbor cells, given a position in the board.
	 */
	public int numberOfNeighborhoodAliveCells(int i, int j) {
		int alive = 0;
		for (int a = i - 1; a <= i + 1; a++) {
			for (int b = j - 1; b <= j + 1; b++) {
				int aux_a = normalize(a);
				int aux_b = normalize(b);
				if (validPosition(aux_a, aux_b)  && (!(aux_a==i && aux_b == j)) && cells[aux_a][aux_b].isAlive()) {
					alive++;
				}
			}
		}
		return alive;
	}

	/**
	 * Checks whether the position (a, b) is within the board bounds.
	 */
	private boolean validPosition(int a, int b) {
		return a >= 0 && a < height && b >= 0 && b < width;
	}

	public void setStrategy(GameStrategy strategy) {
		strategy.setEngine(this);
		this.strategy = strategy;
	}

	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}

	@Override
	public void register(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void notifyObservers() {
		for (Observer observer : observers) {
			observer.update();
		}
	}

}
