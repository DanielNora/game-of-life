package mvc.model;

import mvc.model.strategies.Conway;
import mvc.model.strategies.DayNight;
import mvc.model.strategies.HighLife;
import mvc.model.strategies.LifeWithoutDeath;
import mvc.model.strategies.Seeds;

public enum Strategies {
	CONWAY {
		public GameStrategy getGameStrategy() {
			return new Conway();
		}
	},
	HIGH_LIFE {		
		public GameStrategy getGameStrategy() {
			return new HighLife();
		}
	},
	SEEDS {
		public GameStrategy getGameStrategy() {
			return new Seeds();
		}
	},
	DAY_NIGHT {
		public GameStrategy getGameStrategy() {
			return new DayNight();
		}
	},
	LIFE_WO_DEATH {
		public GameStrategy getGameStrategy() {
			return new LifeWithoutDeath();
		}
	};
	
	public abstract GameStrategy getGameStrategy();
	
	public static GameStrategy fabricate(Strategies strategy) {		
		return strategy.getGameStrategy();
	}
	
	public static Strategies getStrategy(int strategy) throws InvalidStrategyException {
		switch (strategy) {
		case 1: return CONWAY;
		case 2: return HIGH_LIFE;
		case 3: return SEEDS;
		case 4: return DAY_NIGHT;
		case 5: return LIFE_WO_DEATH;
		}
		throw new InvalidStrategyException();
	}
	
	public static boolean validateStrategy(int strategy) {
		int amountOfStrategies = Strategies.values().length;
		
		return (strategy > 0) && (strategy <= amountOfStrategies);
	}
}
