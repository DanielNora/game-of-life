package mvc.model.strategies;

import mvc.model.GameStrategy;
/**
 * LifeWithoutDeath game strategy. The cells never
 * die and 3 alive neighbors are needed for a cell to
 * ressurrect.
 *
 * @author Miguel
 */
public class LifeWithoutDeath extends GameStrategy {
	/**
	 * Returns true for every ever ressurrected cell
	 */
	public boolean shouldKeepAlive(int i, int j) {
		return true;
	}

	public boolean shouldRevive(int i, int j) {
		return engine.numberOfNeighborhoodAliveCells(i, j) == 3;
	}


}
