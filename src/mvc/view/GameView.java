package mvc.view;

import observer.Observer;
import mvc.controller.GameController;
import mvc.model.GameEngine;

public abstract class GameView implements Observer {

	protected GameController controller;
	
	public GameView(GameController controller, GameEngine engine) {
		this.controller = controller;
		engine.register(this);
	}
	
	public abstract void start();
	public abstract void showStatistics();

}
